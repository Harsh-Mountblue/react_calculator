import "./App.css";
import React, { Component } from "react";

class App extends React.Component {
    // constructor(props) {
    //     super(props);

    // }

    state = {
        expression: "",
        checkDot: false,
    };

    equation = (data) => {
        const symbols = "+-*/%";

        if (symbols.includes(data)) {
            this.setState({ checkDot: false });
        }

        // console.log(this.state.expression);
        if (data === "C") {
            this.setState((prevState) => ({
                expression: "",
                checkDot: false
            }));
        } else if (data === "back") {
            if (this.state.expression !== "") {
                if (this.state.expression.slice(-1) === '.') {
                    this.setState({ checkDot: false });
                }
                this.setState({
                    expression: this.state.expression.slice(0, -1),
                });
            }
        } else if (data === "=") {
            if (this.state.expression.length) {
                let temp = this.state.expression.slice(-1);
                if (temp !== "." && !symbols.includes(temp)) {
                    this.setState({
                        expression: eval(this.state.expression).toString(),
                    });
                    if (this.state.expression.includes(".")) {
                        this.setState({ checkDot: true });
                    } else {
                        this.setState({ checkDot: false });
                    }
                }
            }
        } else if (data === ".") {
            if (this.state.checkDot === false) {
                this.setState({ expression: this.state.expression + data });
                this.setState({ checkDot: true });
            }
        } else {
            // console.log(this.state.expression.length);
            // console.log(this.state.expression);
            if (this.state.expression.length) {
                let temp = this.state.expression.slice(-1);
                if (!(symbols.includes(temp) && symbols.includes(data))) {
                    this.setState({ expression: this.state.expression + data });
                }
            } else if (data === "-") {
                this.setState({ expression: this.state.expression + data });
            } else {
                if (!symbols.includes(data))
                    this.setState({ expression: this.state.expression + data });
            }
        }
        // console.log(this.state.expression);
    };

    render() {
        return (
            <div className="app">
                <div className="display">
                    <h1> {this.state.expression} </h1>
                </div>
                <div className="row1">
                    <div
                        className="column"
                        onClick={() => {
                            this.equation("C");
                        }}
                    >
                        C
                    </div>
                    <div
                        className="column"
                        onClick={() => {
                            this.equation("back");
                        }}
                    >
                        &lt;-
                    </div>
                    <div
                        className="column"
                        onClick={() => {
                            this.equation("%");
                        }}
                    >
                        %
                    </div>
                    <div
                        className="column"
                        onClick={() => {
                            this.equation("/");
                        }}
                    >
                        /
                    </div>
                </div>
                <div className="row2">
                    <div
                        className="column"
                        onClick={() => {
                            this.equation("7");
                        }}
                    >
                        7
                    </div>
                    <div
                        className="column"
                        onClick={() => {
                            this.equation("8");
                        }}
                    >
                        8
                    </div>
                    <div
                        className="column"
                        onClick={() => {
                            this.equation("9");
                        }}
                    >
                        9
                    </div>
                    <div
                        className="column"
                        onClick={() => {
                            this.equation("*");
                        }}
                    >
                        ×
                    </div>
                </div>
                <div className="row3">
                    <div
                        className="column"
                        onClick={() => {
                            this.equation("4");
                        }}
                    >
                        4
                    </div>
                    <div
                        className="column"
                        onClick={() => {
                            this.equation("5");
                        }}
                    >
                        5
                    </div>
                    <div
                        className="column"
                        onClick={() => {
                            this.equation("6");
                        }}
                    >
                        6
                    </div>
                    <div
                        className="column"
                        onClick={() => {
                            this.equation("-");
                        }}
                    >
                        -
                    </div>
                </div>
                <div className="row4">
                    <div
                        className="column"
                        onClick={() => {
                            this.equation("1");
                        }}
                    >
                        1
                    </div>
                    <div
                        className="column"
                        onClick={() => {
                            this.equation("2");
                        }}
                    >
                        2
                    </div>
                    <div
                        className="column"
                        onClick={() => {
                            this.equation("3");
                        }}
                    >
                        3
                    </div>
                    <div
                        className="column"
                        onClick={() => {
                            this.equation("+");
                        }}
                    >
                        +
                    </div>
                </div>
                <div className="row5">
                    {/* <div className="column">More</div> */}
                    <div
                        className="column0"
                        onClick={() => {
                            this.equation("0");
                        }}
                    >
                        0
                    </div>
                    <div
                        className="column"
                        onClick={() => {
                            this.equation(".");
                        }}
                    >
                        .
                    </div>
                    <div
                        className="column"
                        onClick={() => {
                            this.equation("=");
                        }}
                    >
                        =
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
